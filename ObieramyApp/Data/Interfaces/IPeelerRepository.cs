﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ObieramyApp.Data.Models;

namespace ObieramyApp.Data.Interfaces
{
    public interface IPeelerRepository
    {
        IEnumerable<Peeler> Peelers { get; }

        Peeler GetPeelerById(int peelerId);

    }
}
