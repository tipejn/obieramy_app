﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ObieramyApp.Data.Models;

namespace ObieramyApp.Data.Interfaces
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> Categories { get; }
    }
}
