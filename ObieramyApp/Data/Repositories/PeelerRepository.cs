﻿using Microsoft.EntityFrameworkCore;
using ObieramyApp.Data.Interfaces;
using ObieramyApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObieramyApp.Data.Repositories
{
    public class PeelerRepository : IPeelerRepository
    {
        private readonly AppDbContext _appDbContext;

        public PeelerRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Peeler> Peelers => _appDbContext.Peelers.Include(c => c.Category);

        public Peeler GetPeelerById(int peelerId) => _appDbContext.Peelers.FirstOrDefault(p => p.PeelerId == peelerId);
    }
}
