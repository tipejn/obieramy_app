﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ObieramyApp.Data.Interfaces;
using ObieramyApp.Data.Models;

namespace ObieramyApp.Data.Mocks
{
    public class MockPeelerRepository : IPeelerRepository
    {
        private readonly ICategoryRepository _categoryRepository = new MockCategoryRepository();
        public IEnumerable<Peeler> Peelers
        {
            get
            {
                return new List<Peeler>
                {
                    new Peeler
                    {
                        Name = "OPP-001",
                        Price = 599.00M,
                        ShortDescription = "Obieraczka do owoców OPP-001",
                        LongDescription = "Innowacyjna obieraczka do owoców. Jedyna w Polsce.",
                        Category = _categoryRepository.Categories.First(),
                        ImageUrl = "",
                        ImageThumbnailUrl = "https://cdn.shoplo.com/8416/products/th640/aaaj/86-5.png",
                        InStock = 10

                    }
                };
            }
        }

        public Peeler GetPeelerById(int peelerId)
        {
            throw new NotImplementedException();
        }
    }
}
