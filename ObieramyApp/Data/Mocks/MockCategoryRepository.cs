﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ObieramyApp.Data.Interfaces;
using ObieramyApp.Data.Models;

namespace ObieramyApp.Data.Mocks
{
    public class MockCategoryRepository : ICategoryRepository
    {
        public IEnumerable<Category> Categories
        {
            get
            {
                return new List<Category>
                {
                    new Category {CategoryName = "Fruit Peelers", Description = "Fruit Peelers"},
                    new Category {CategoryName = "Nuts Peelers", Description = "Nuts Peelers"},
                    new Category {CategoryName = "Bean Peelers", Description = "Bean Peelers"}
                };
            }
        }

    }
}
