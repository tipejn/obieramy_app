﻿using Microsoft.EntityFrameworkCore;
using ObieramyApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObieramyApp.Data
{
    public class AppDbContext: DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
        {
        }

        public DbSet<Peeler> Peelers { get; set; }

        public DbSet<Category> Categories { get; set; }


    }
}
