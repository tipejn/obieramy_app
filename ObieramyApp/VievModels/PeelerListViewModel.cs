﻿using ObieramyApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObieramyApp.VievModels
{
    public class PeelerListViewModel
    {
        public IEnumerable<Peeler> Peelers { get; set; }

        public string CurrentCategory { get; set; } 

    }
}
 