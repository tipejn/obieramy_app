﻿using Microsoft.AspNetCore.Mvc;
using ObieramyApp.Data.Interfaces;
using ObieramyApp.VievModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObieramyApp.Controllers
{
    public class PeelerController: Controller
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IPeelerRepository _peelerRepository;

        public PeelerController(ICategoryRepository categoryRepository, IPeelerRepository peelerRepository)
        {
            _categoryRepository = categoryRepository;
            _peelerRepository = peelerRepository;
        }

        public ViewResult List()
        {
            ViewBag.Name = "TP";
            PeelerListViewModel vm = new PeelerListViewModel();
            vm.Peelers = _peelerRepository.Peelers;
            vm.CurrentCategory = "PeelerCategory";
            return View(vm);
        }
    }
}
